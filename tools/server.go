package main

import (
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Args[1]
	dir := os.Args[2]
	log.Printf("listening on %q...", port)
	err := http.ListenAndServe(port, http.FileServer(http.Dir(dir)))
	log.Fatalln(err)
}
