#### Currently working on podcasts pagination
Need to implement router.go TODO because the pagniation links wrap SVGElement and that is what is passed on as the click event target. I need to be able to go up the tree until I reach the <a> that caused the click.

Need to implement a special view object in content/podcasts.go where I can precompute the page links, specially the previous and next links. Even though Go has variable support, it only seems to support variale assignments. But if I try to do arithmetic operations on an int variable, it fails to parse.

Need to add more podcasts to the models/podcasts file so I can test out pagination.
