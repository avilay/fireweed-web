package framework

import (
	"regexp"
	"log"
	"syscall/js"
	neturl "net/url"
)

// Router to hold mappings from routes, which are regex patterns to their handlers.
// rootHandler is treated specially and has no entry into the routes array because
// the root path "/" will match all routes. We want the router to ignore routes
// it does not know about because there will be some other component that might
// be handling it.
type Router struct {
	routes []*regexp.Regexp
	handlers []func(string)
	rootHandler func()
	callbacks []js.Callback
}

// RegisterRoute ...
func (router *Router) RegisterRoute(route string, handler func(string)) {
	re := regexp.MustCompile(route)
	router.routes = append(router.routes, re)
	router.handlers = append(router.handlers, handler)
}

// RegisterRootRoute ---
func (router *Router) RegisterRootRoute(handler func()) {
	router.rootHandler = handler
}

// OnLinkClick ...
func (router *Router) onLinkClick(event js.Value) {
	target := event.Get("target")
	if target.String() == "null" || target.String() == "undefined" {
		log.Println("Property target does not exist!")
		target = js.ValueOf("/")
	}

	// TODO: If the target is not <a>, go up the tree until I get to an <a>
	url := target.String()
	var path string
	urlObj, err := neturl.Parse(url)
	if err != nil {
		log.Printf("Unable to parse url %s: %v\n", url, err)
		path = "/"
	} else {
		path = urlObj.Path
	}
	log.Println("path ", path)

	handlerFound := false
	// If the path is root, then do not look in the routes array
	if path == "/" {
		router.rootHandler()
		handlerFound = true
	} else {
		for i, route := range router.routes {
			matches := route.FindStringSubmatch(path)
			if len(matches) > 0 {
				log.Println("Found match: ", route)
				router.handlers[i](url)
				handlerFound = true
				break
			}
		}
	}

	if handlerFound {
		router.RegisterLinkClicks()
	} else {
		log.Println("Unable to find a route for path ", path)
	}
}

var singleton = new(Router)

// RegisterLinkClicks ---
func (router *Router) RegisterLinkClicks() {
	// First unregister all old a tag click events
	for _, callback := range(router.callbacks) {
		callback.Release()
	}
	router.callbacks = nil

	// Now register onclick callbacks for a tags in the current DOM
	log.Println("Setting click events")
	atags := js.Global().Get("document").Get("body").Call("querySelectorAll", "a")
	for i := 0; i < atags.Length(); i++ {
		callback := js.NewEventCallback(js.PreventDefault, router.onLinkClick)
		atag := atags.Index(i)
		atag.Set("onclick", callback)
		router.callbacks = append(router.callbacks, callback)
	}
}

// NewRouter ...
func NewRouter() *Router {
	return singleton
}
