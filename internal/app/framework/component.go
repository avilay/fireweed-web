package framework

// Component ...
type Component interface {
	Mount(tagID string)
	UnMount()
}
