package models

import (
	"math/rand"
)

// Podcast ...
type Podcast struct {
	ID                             int
	Title, Author, Link, Thumbnail string
}

// Podcasts ---
type Podcasts struct {
	Page int
	TotalPages int
	Items []Podcast
}

var allPodcasts = []Podcast{
	{
		ID:        1,
		Link:      "/podcasts/1",
		Title:     "Talk Python To Me - Python conve ...",
		Author:    "Michael Kennedy",
		Thumbnail: "https://talkpython.fm/static/img/podcast-theme-img_1400_v3.png",
	},
	{
		ID:        2,
		Link:      "/podcasts/2",
		Title:     "Data Skeptic",
		Author:    "Kyle Polich",
		Thumbnail: "http://static.libsyn.com/p/assets/2/9/3/8/2938570bb173ccbc/DataSkeptic-Podcast-1A.jpg",
	},
	{
		ID:        3,
		Link:      "/podcasts/3",
		Title:     "Software Engineering Radio - The ...",
		Author:    "SE-Radio Team (team@ ...",
		Thumbnail: "http://media.computer.org/sponsored/podcast/se-radio/se-radio-logo-1400x1475.jpg",
	},
	{
		ID:        4,
		Link:      "/podcasts/4",
		Title:     "AWS Podcast",
		Author:    "Amazon Web Services ...",
		Thumbnail: "http://d3gih7jbfe3jlq.cloudfront.net/AWS-Podcast-Title-Art.jpg",
	},
	{
		ID:        5,
		Link:      "/podcasts/5",
		Title:     "Science Magazine Podcast",
		Author:    "Science Magazine",
		Thumbnail: "http://sciencemag.org/sites/default/files/science1400.png",
	},
	{
		ID:        6,
		Link:      "/podcasts/6",
		Title:     "a16z",
		Author:    "a16z",
		Thumbnail: "http://i1.sndcdn.com/avatars-000073120599-46q7im-original.jpg",
	},
	{
		ID:        7,
		Link:      "/podcasts/7",
		Title:     "alking Machines",
		Author:    "Tote Bag Productions",
		Thumbnail: "https://content.production.cdn.art19.com/images/44/39/e9/68/4439e968-d3f7-48c2-8158-146c43a73426/1aebbf35ec5a7850cf308d730985ca70135e8b2392cc3028a8350a67c9cc9c583a91ccfeb90b6cdb57244727903fad715d2acf79eea0268affedff4f68283592.jpeg",
	},
	{
		ID:        8,
		Link:      "/podcasts/8",
		Title:     "Software Engineering Daily",
		Author:    "softwareengineeringd ...",
		Thumbnail: "http://softwaredaily.wpengine.com/wp-content/uploads/powerpress/SED_square_solid_bg.png",
	},
	{
		ID:        9,
		Link:      "/podcasts/9",
		Title:     "Data Crunch | Artificial Intelli ...",
		Author:    "ginette@vaultanalyti ...",
		Thumbnail: "https://vaultanalytics.com/wp-content/uploads/2017/12/Data_Crunch_Logo_IG.jpg",
	},
	{
		ID:        10,
		Link:      "/podcasts/10",
		Title:     "Learning Machines 101",
		Author:    "Richard M. Golden",
		Thumbnail: "http://static.libsyn.com/p/assets/a/9/2/a/a92a67d6fab3c596/CoverArt1400by1400.jpg",
	},
	{
		ID:        11,
		Link:      "/podcasts/11",
		Title:     "Machine Learning – Software Engi ...",
		Author:    "softwareengineeringd ...",
		Thumbnail: "http://softwareengineeringdaily.com/wp-content/uploads/powerpress/show_ml.jpg",
	},
	{
		ID:        12,
		Link:      "/podcasts/12",
		Title:     "Machine Learning",
		Author:    "Andrew Ng",
		Thumbnail: "http://a1415.phobos.apple.com/us/r30/Podcasts/v4/2f/7e/56/2f7e563b-629b-bf69-9168-2f85fd58427f/mza_6931836784187099205.jpg",
	},
	{
		ID:        13,
		Link:      "/podcasts/13",
		Title:     "This Week in Machine Learning & ...",
		Author:    "TWiML & AI",
		Thumbnail: "http://static.libsyn.com/p/assets/d/1/f/a/d1fa0cd9db1a1e58/avatars-000328798255-7rxgio-original.jpg",
	},
	{
		ID:        14,
		Link:      "/podcasts/14",
		Title:     "The Vocabulary of Big Data",
		Author:    "N. Saffell (ns480@ca ...",
		Thumbnail: "http://rss.sms.cam.ac.uk/itunes-image/2029256.jpg",
	},
	{
		ID:        15,
		Link:      "/podcasts/15",
		Title:     "Machine Learning Guide",
		Author:    "Tyler Renelle",
		Thumbnail: "http://static.libsyn.com/p/assets/0/2/a/3/02a3ac1ef30f67a1/art.jpg",
	},
}

// GetPodcasts ...
func GetPodcasts(page int) Podcasts {
	var podcasts Podcasts
	if page == 1 {
		podcasts = Podcasts{Page: 1, TotalPages: 2, Items: allPodcasts[:10]}
	} else if page == 2 {
		podcasts = Podcasts{Page: 2, TotalPages: 2, Items: allPodcasts[10:]}
	}
	return podcasts
}

// Search ---
func Search(query string) Podcasts {
	podcasts := GetPodcasts(1)
	rand.Shuffle(len(podcasts.Items), func(i, j int) {
		podcasts.Items[i], podcasts.Items[j] = podcasts.Items[j], podcasts.Items[i]
	})
	return podcasts
}
