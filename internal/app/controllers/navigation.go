package controllers

import (
	"bitbucket.org/avilay/fireweed-web/internal/templates"
	"fmt"
	"strings"
	"syscall/js"
)

// Navigation ...
type Navigation struct {
	templateFile string
	tagID string

	ActiveLink string
	IsLoggedIn bool
	menuItems  []MenuItem
}

// MenuItem ---
type MenuItem struct {
	NavID, Text, Link string
	isActive          bool
}

// Mount ...
func (n *Navigation) Mount(tagID string) {
	n.tagID = tagID
	tmpl := templates.LoadTemplate(n.templateFile)
	bldr := new(strings.Builder)

	podcasts := MenuItem{NavID: "podcasts", Text: "Podcasts", Link: "/podcasts", isActive: true}
	n.menuItems = append(n.menuItems, podcasts)
	if n.IsLoggedIn {
		playlist := MenuItem{NavID: "playlist", Text: "Playlist", Link: "/playlist", isActive: false}
		n.menuItems = append(n.menuItems, playlist)
		favorites := MenuItem{NavID: "favorites", Text: "Favorites", Link: "/favorites", isActive: false}
		n.menuItems = append(n.menuItems, favorites)
	}
	tmpl.Execute(bldr, n.menuItems)
	html := bldr.String()
	js.Global().Get("document").Call("getElementById", tagID).Set("innerHTML", html)
}

// UnMount ...
func (n *Navigation) UnMount() {
	js.Global().Get("document").Call("getElementById", n.tagID).Set("innerHTML", "")
	n.tagID = ""
}

// ActivateLink ...
func (n *Navigation) ActivateLink(link string) {
	dataNavSelector := fmt.Sprintf("[data-nav=\"%s\"]", link)
	li := js.Global().Get("document").Get("body").Call("querySelector", dataNavSelector)
	li.Call("setAttribute", "class", "uk-active")
}

// NewNavigation ---
func NewNavigation() *Navigation {
	nav := new(Navigation)
	nav.templateFile = "nav.html"
	return nav
}
