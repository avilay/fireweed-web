package controllers

import (
	"bitbucket.org/avilay/fireweed-web/internal/app/controllers/content"
	"bitbucket.org/avilay/fireweed-web/internal/app/framework"
	"log"
)

// Root ---
type Root struct {
	tagID string
	router *framework.Router

	nav *Navigation
	footer *Footer

	// Content
	content framework.Component
	welcome *content.Welcome
	podcasts *content.Podcasts
}

func (r *Root) onPodcastsClick(url string) {
	// TODO: If there is a ?page=n in the query string
	page := 1
	r.content.UnMount()
	r.content = r.podcasts
	r.podcasts.Page(page)
	r.podcasts.Mount("content")
	r.nav.ActivateLink("podcasts")
}

// Mount ---
func (r *Root) Mount(tagID string) {
	r.tagID = tagID
	log.Println("Root::Mount")
	r.nav.Mount("nav")
	r.footer.Mount("footer")
	r.welcome.Mount("content")
	r.content = r.welcome

	r.router.RegisterRoute(`/podcasts`, r.onPodcastsClick)
}

// UnMount ---
func (r *Root) UnMount() {
	// Root will never be unmounted
	log.Println("Root::UnMount")
	panic("root cannot be unmounted")
}

// NewRoot ---
func NewRoot(router *framework.Router) *Root {
	root := new(Root)
	root.router = router
	root.nav = NewNavigation()
	root.footer = NewFooter()
	root.welcome = content.NewWelcome()
	root.podcasts = content.NewPodcasts()
	return root
}