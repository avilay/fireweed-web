package content

import (
	"bitbucket.org/avilay/fireweed-web/internal/templates"
	"log"
	"strings"
	"syscall/js"

	"bitbucket.org/avilay/fireweed-web/internal/app/models"
)

// Podcasts ...
type Podcasts struct {
	templateFile string
	podcasts     models.Podcasts
	callbacks    []js.Callback
	tagID        string
}

// Page ---
func (p *Podcasts) Page(num int) {
	p.podcasts = models.GetPodcasts(num)
}

// Mount ...
func (p *Podcasts) Mount(tagID string) {
	log.Println("Podcasts::Mount")
	p.tagID = tagID
	tmpl := templates.LoadTemplate(p.templateFile)
	bldr := new(strings.Builder)
	tmpl.Execute(bldr, p.podcasts)
	html := bldr.String()
	js.Global().Get("document").Call("getElementById", tagID).Set("innerHTML", html)

	p.registerEvents()
}

// OnSearch ---
func (p *Podcasts) OnSearch(event js.Value) {
	log.Println("OnSearch")
	query := js.Global().Get("document").Call("getElementById", "searchbox").Get("value")
	p.podcasts = models.Search(query.String())
	p.Mount(p.tagID)
	searchBox := js.Global().Get("document").Call("getElementById", "searchbox")
	searchBox.Set("value", query)
}

func (p *Podcasts) registerEvents() {
	searchCallback := js.NewEventCallback(js.PreventDefault, p.OnSearch)
	form := js.Global().Get("document").Get("body").Call("querySelector", "form")
	form.Set("onsubmit", searchCallback)
	p.callbacks = append(p.callbacks, searchCallback)
}

// UnMount ---
func (p *Podcasts) UnMount() {
	for _, callback := range p.callbacks {
		callback.Release()
	}
	p.callbacks = nil

	js.Global().Get("document").Call("getElementById", p.tagID).Set("innerHTML", "")
	p.tagID = ""
}

// NewPodcasts ---
func NewPodcasts() *Podcasts {
	pcasts := new(Podcasts)
	pcasts.templateFile = "podcasts.html"
	return pcasts
}
