package content

import (
	"bitbucket.org/avilay/fireweed-web/internal/templates"
	"syscall/js"
	"strings"
)

// Welcome ---
type Welcome struct {
	templateFile string
	tagID string
}

// Mount ---
func (w *Welcome) Mount(tagID string) {
	w.tagID = tagID
	tmpl := templates.LoadTemplate(w.templateFile)
	bldr := new(strings.Builder)
	tmpl.Execute(bldr, nil)
	html := bldr.String()
	js.Global().Get("document").Call("getElementById", tagID).Set("innerHTML", html)

}

// UnMount ---
func (w *Welcome) UnMount() {
	js.Global().Get("document").Call("getElementById", w.tagID).Set("innerHTML", "")
	w.tagID = ""
}

// NewWelcome ---
func NewWelcome() *Welcome {
	welcome := new(Welcome)
	welcome.templateFile = "welcome.html"
	return welcome
}

