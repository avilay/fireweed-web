package controllers

import (
	"bitbucket.org/avilay/fireweed-web/internal/templates"
	"strings"
	"syscall/js"
)

// Footer ---
type Footer struct {
	templateFile string
	tagID string
}

// Mount ---
func (f *Footer) Mount(tagID string) {
	f.tagID = tagID
	tmpl := templates.LoadTemplate(f.templateFile)
	bldr := new(strings.Builder)
	tmpl.Execute(bldr, nil)
	html := bldr.String()
	js.Global().Get("document").Call("getElementById", tagID).Set("innerHTML", html)
}

// UnMount ---
func (f *Footer) UnMount() {
	js.Global().Get("document").Call("getElementById", f.tagID).Set("innerHTML", "")
	f.tagID = ""
}

// NewFooter ---
func NewFooter() *Footer {
	footer := new(Footer)
	footer.templateFile = "footer.html"
	return footer
}
