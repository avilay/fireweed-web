package main

import (
	"log"

	"bitbucket.org/avilay/fireweed-web/internal/app/controllers"
	"bitbucket.org/avilay/fireweed-web/internal/app/framework"
)

func main() {
	dummy := make(chan struct{})

	log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile)
	log.Println("Starting app..")
	log.Println("Listening...")

	router := framework.NewRouter()
	root := controllers.NewRoot(router)
	root.Mount("")


	router.RegisterRootRoute(func() {root.Mount("")})
	router.RegisterLinkClicks()

	<-dummy
}
