package templates

import (
	"fmt"
	"html/template"
	"github.com/gobuffalo/packr"
)

var box = packr.NewBox(".")

// LoadTemplate ---
func LoadTemplate(filename string) *template.Template {
	html, err := box.FindString(filename)
	if err != nil {
		panic(fmt.Sprintf("%s does not exist", filename))
	}
	tmpl, err := template.New("nav").Parse(html)
	if err != nil {
		panic(fmt.Sprintf("%s is not parseable", filename))
	}
	return tmpl
}
