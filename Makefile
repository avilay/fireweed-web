PRJ_HOME := bitbucket.org/avilay/fireweed-web
OUT_WASM := spa.wasm

build:
	GOOS=js GOARCH=wasm packr build -o web/$(OUT_WASM) $(PRJ_HOME)/internal/cmd/spa


clean:
	rm web/$(OUT_WASM)
	go clean $(PRJ_HOME)/internal/cmd/spa
	go clean $(PRJ_HOME)/internal/app/framework

all: clean build

run: build
	go run tools/server.go :8080 web
